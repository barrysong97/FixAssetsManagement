package org.jeecg.modules.demo.fixassets.controller;


import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.demo.fixassets.entity.PieChart;
import org.jeecg.modules.demo.fixassets.mapper.AssetMapper;
import org.jeecg.modules.demo.fixassets.model.AssetsValueChart;
import org.jeecg.modules.demo.fixassets.service.IAssetService;
import org.jeecg.modules.demo.fixassets.service.ICategoriesService;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@Api(tags = "chartcs")
@RestController
@RequestMapping("/fixassets/asset")
@Slf4j
public class DataChartController {

    @Autowired
    private ISysDepartService sysDepartService;

    @Autowired
    private ICategoriesService categoriesService;

    @Autowired
    private IAssetService assetService;

    @Autowired
    private AssetMapper assetMapper;

    @GetMapping("/chartData")
    public Result<?> getChartData() {
        List<PieChart> pieChartDeparts = assetMapper.getPieDepart();
        List<String> columnDepart = Arrays.asList("部门名称", "资产价值");
        List<Map<String, String>> rowDepart = new ArrayList<>();
        pieChartDeparts.forEach(v -> {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("部门名称", v.getName());
            map.put("资产价值", v.getValue());
            rowDepart.add(map);
        });

        //category
        List<PieChart> pieChartCategory = assetMapper.getPieCategory();
        List<String> columnCategory = Arrays.asList("分类名称", "资产价值");
        List<Map<String, String>> rowCategory = new ArrayList<>();
        pieChartCategory.forEach(v -> {
            Map<String, String> map = new HashMap<>();
            map.put("分类名称", v.getName());
            map.put("资产价值", v.getValue());
            rowCategory.add(map);
        });
        Map<String, Object> result = new HashMap<>();
        result.put("depart", new AssetsValueChart(columnDepart, rowDepart));
        result.put("category", new AssetsValueChart(columnCategory, rowCategory));
        result.put("sumValue", assetMapper.sumValue());
        result.put("maxValue", assetMapper.maxValue());
        result.put("minValue", assetMapper.minValue());
        result.put("maxDepart", assetMapper.maxDepart());
        result.put("minDepart", assetMapper.minDepart());
        result.put("categoryNum", assetMapper.categoryNum());
        result.put("bigCategory", assetMapper.maxCategory());
        result.put("minCategory", assetMapper.minCategory());
        return Result.ok(result);
    }
}
