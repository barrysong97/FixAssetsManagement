package org.jeecg.modules.demo.fixassets.model;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class AssetsValueChart {


    private List<String> columns;
    private List<Map<String, String>> rows;

    public AssetsValueChart(List<String> columns, List<Map<String, String>> rows) {
        this.columns = columns;
        this.rows = rows;
    }

    public AssetsValueChart() {
    }
}
