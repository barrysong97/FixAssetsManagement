package org.jeecg.modules.demo.fixassets.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.common.constant.CacheConstant;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.fixassets.entity.Category;
import org.jeecg.modules.demo.fixassets.mapper.CategoriesMapper;

import org.jeecg.modules.demo.fixassets.model.CategoryIdModal;
import org.jeecg.modules.demo.fixassets.model.SysCategoryTreeModel;
import org.jeecg.modules.demo.fixassets.service.ICategoriesService;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.model.DepartIdModel;
import org.jeecg.modules.system.model.SysDepartTreeModel;
import org.jeecg.modules.system.util.FindsDepartsChildrenUtil;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 分类表
 * @Author: jeecg-boot
 * @Date:   2020-04-02
 * @Version: V1.0
 */
@Service
public class CategoriesServiceImpl extends ServiceImpl<CategoriesMapper, Category> implements ICategoriesService {

	@Override
	public void addCategory(Category category) {
		if(oConvertUtils.isEmpty(category.getPid())){
			category.setPid(ICategoriesService.ROOT_PID_VALUE);
		}else{
			//如果当前节点父ID不为空 则设置父节点的hasChildren 为1
			Category parent = baseMapper.selectById(category.getPid());
			if(parent!=null && !"1".equals(parent.getHasChild())){
				parent.setHasChild("1");
				baseMapper.updateById(parent);
			}
		}
		baseMapper.insert(category);
	}
	
	@Override
	public void updateCategory(Category category) {
		Category entity = this.getById(category.getId());
		if(entity==null) {
			throw new JeecgBootException("未找到对应实体");
		}
		String old_pid = entity.getPid();
		String new_pid = category.getPid();
		if(!old_pid.equals(new_pid)) {
			updateOldParentNode(old_pid);
			if(oConvertUtils.isEmpty(new_pid)){
				category.setPid(ICategoriesService.ROOT_PID_VALUE);
			}
			if(!ICategoriesService.ROOT_PID_VALUE.equals(category.getPid())) {
				baseMapper.updateTreeNodeStatus(category.getPid(), ICategoriesService.HASCHILD);
			}
		}
		baseMapper.updateById(category);
	}
	
	@Override
	public void deleteCategory(String id) throws JeecgBootException {
		Category category = this.getById(id);
		if(category==null) {
			throw new JeecgBootException("未找到对应实体");
		}
		updateOldParentNode(category.getPid());
		baseMapper.deleteById(id);
	}

	/**
	 * queryTreeList 对应 queryTreeList 查询所有的部门数据,以树结构形式响应给前端
	 */

	@Override
	public List<SysCategoryTreeModel> queryTreeList() {
		LambdaQueryWrapper<Category> query = new LambdaQueryWrapper<Category>();
		query.eq(Category::getPid, 0);
		List<Category> list = this.list(query);
		// 调用wrapTreeDataToTreeList方法生成树状数据
		List<SysCategoryTreeModel> listResult = wrapTreeDataToTreeList(list);
		return listResult;
	}

	public  List<SysCategoryTreeModel> wrapTreeDataToTreeList(List<Category> recordList) {

		List<SysCategoryTreeModel> records = new ArrayList<>();

		recordList.forEach(v -> {
			records.add(findChildren(v));
		});

		return records;
	}

	private SysCategoryTreeModel findChildren(Category record) {
		SysCategoryTreeModel sysCategoryTreeModel = new SysCategoryTreeModel(record);
		if (!Objects.equals(record.getHasChild(), "1")) {
			return sysCategoryTreeModel;
		}

		List<SysCategoryTreeModel> children = new ArrayList<>();
		LambdaQueryWrapper<Category> query = new LambdaQueryWrapper<Category>();
		query.eq(Category::getPid, record.getId());
		List<Category> list = this.list(query);
		list.forEach(v -> {
			children.add(findChildren(v));
		});
		sysCategoryTreeModel.getChildren().addAll(children);
		return sysCategoryTreeModel;
	}


	/**
	 * 根据所传pid查询旧的父级节点的子节点并修改相应状态值
	 * @param pid`
	 */
	private void updateOldParentNode(String pid) {
		if(!ICategoriesService.ROOT_PID_VALUE.equals(pid)) {
			Integer count = baseMapper.selectCount(new QueryWrapper<Category>().eq("pid", pid));
			if(count==null || count<=1) {
				baseMapper.updateTreeNodeStatus(pid, ICategoriesService.NOCHILD);
			}
		}
	}

}
