package org.jeecg.modules.demo.fixassets.entity;

import lombok.Data;

@Data
public class PieChart {

    private String name;
    private String value;
}
