package org.jeecg.modules.demo.fixassets.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.demo.fixassets.entity.Asset;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.demo.fixassets.entity.PieChart;

/**
 * @Description: 固定资产表
 * @Author: jeecg-boot
 * @Date:   2020-04-02
 * @Version: V1.0
 */
public interface AssetMapper extends BaseMapper<Asset> {

    @Select("select sum(a.value * a.num) as 'value', d.depart_name as `name` from asset a, sys_depart d where a.department = d.id group by d.id")
    List<PieChart> getPieDepart();

    @Select("select sum(a.value * a.num) as 'value', c.name as `name` from asset a, category c where a.category = c.id group by c.id")
    List<PieChart> getPieCategory();

    @Select("select sum(a.value * a.num) from asset a")
    String sumValue();

    @Select("select max(a.value * a.num) from asset a")
    String maxValue();

    @Select("select min(a.value * a.num) from asset a")
    String minValue();

    @Select("select sum(a.value * a.num) as 'value' from asset a, sys_depart d where a.department = d.id group by d.id ORDER BY VALUE DESC  limit 1")
    String maxDepart();

    @Select("select sum(a.value * a.num) as 'value' from asset a, sys_depart d where a.department = d.id group by d.id ORDER BY VALUE ASC limit 1")
    String minDepart();

    @Select("select count(c.pid) from category c where c.pid != 0")
    String minCategory();

    @Select("select count(c.pid) from category c where c.pid = 0")
    String maxCategory();

    @Select("select count(c.name) from category c")
    String categoryNum();
}
