package org.jeecg.modules.demo.fixassets.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.fixassets.entity.Asset;
import org.jeecg.modules.demo.fixassets.service.IAssetService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.demo.fixassets.service.ICategoriesService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 固定资产表
 * @Author: jeecg-boot
 * @Date:   2020-04-02
 * @Version: V1.0
 */
@Api(tags="固定资产表")
@RestController
@RequestMapping("/fixassets/asset")
@Slf4j
public class AssetController extends JeecgController<Asset, IAssetService> {
	@Autowired
	private IAssetService assetService;

	@Autowired
	private ICategoriesService categoriesService;
	
	/**
	 * 分页列表查询
	 *
	 * @param asset
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "固定资产表-分页列表查询")
	@ApiOperation(value="固定资产表-分页列表查询", notes="固定资产表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(Asset asset,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Asset> queryWrapper = QueryGenerator.initQueryWrapper(asset, req.getParameterMap());
		Page<Asset> page = new Page<Asset>(pageNo, pageSize);
		IPage<Asset> pageList = assetService.page(page, queryWrapper);
		pageList.getRecords().forEach(v -> {
			v.setCategory(categoriesService.getById(v.getCategory()).getName());
		});
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param asset
	 * @return
	 */
	@AutoLog(value = "固定资产表-添加")
	@ApiOperation(value="固定资产表-添加", notes="固定资产表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody Asset asset) {
		assetService.save(asset);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param asset
	 * @return
	 */
	@AutoLog(value = "固定资产表-编辑")
	@ApiOperation(value="固定资产表-编辑", notes="固定资产表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody Asset asset) {
		assetService.updateById(asset);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "固定资产表-通过id删除")
	@ApiOperation(value="固定资产表-通过id删除", notes="固定资产表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		assetService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "固定资产表-批量删除")
	@ApiOperation(value="固定资产表-批量删除", notes="固定资产表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.assetService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "固定资产表-通过id查询")
	@ApiOperation(value="固定资产表-通过id查询", notes="固定资产表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		Asset asset = assetService.getById(id);
		if(asset==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(asset);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param asset
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Asset asset) {
        return super.exportXls(request, asset, Asset.class, "固定资产表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Asset.class);
    }

}
