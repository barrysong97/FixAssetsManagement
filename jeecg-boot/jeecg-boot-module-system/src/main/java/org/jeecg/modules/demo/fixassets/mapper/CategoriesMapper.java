package org.jeecg.modules.demo.fixassets.mapper;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.fixassets.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 分类表
 * @Author: jeecg-boot
 * @Date:   2020-04-02
 * @Version: V1.0
 */
public interface CategoriesMapper extends BaseMapper<Category> {

	/**
	 * 编辑节点状态
	 * @param id
	 * @param status
	 */
	void updateTreeNodeStatus(@Param("id") String id, @Param("status") String status);

}
