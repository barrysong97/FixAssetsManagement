package org.jeecg.modules.demo.fixassets.service;

import org.jeecg.modules.demo.fixassets.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.demo.fixassets.model.SysCategoryTreeModel;

import java.util.List;

/**
 * @Description: 分类表
 * @Author: jeecg-boot
 * @Date:   2020-04-02
 * @Version: V1.0
 */
public interface ICategoriesService extends IService<Category> {

	/**根节点父ID的值*/
	public static final String ROOT_PID_VALUE = "0";
	
	/**树节点有子节点状态值*/
	public static final String HASCHILD = "1";
	
	/**树节点无子节点状态值*/
	public static final String NOCHILD = "0";

	/**新增节点*/
	void addCategory(Category category);
	
	/**修改节点*/
	void updateCategory(Category category) throws JeecgBootException;
	
	/**删除节点*/
	void deleteCategory(String id) throws JeecgBootException;

	List<SysCategoryTreeModel> queryTreeList();
}
