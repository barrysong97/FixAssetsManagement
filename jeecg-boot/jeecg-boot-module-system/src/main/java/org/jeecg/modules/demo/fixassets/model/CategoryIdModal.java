package org.jeecg.modules.demo.fixassets.model;

import lombok.Data;
import org.jeecg.modules.demo.fixassets.entity.Category;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.model.DepartIdModel;
import org.jeecg.modules.system.model.SysDepartTreeModel;

import java.util.ArrayList;
import java.util.List;

@Data
public class CategoryIdModal {

    private static final long serialVersionUID = 1L;

    // 主键ID
    private String key;

    // 主键ID
    private String value;

    // 部门名称
    private String title;

    List<CategoryIdModal> children = new ArrayList<>();

    /**
     * 将SysDepartTreeModel的部分数据放在该对象当中
     * @param
     * @return
     */
    public CategoryIdModal convert(SysCategoryTreeModel category) {
        this.key = category.getId();
        this.value = category.getId();
        this.title = category.getName();
        return this;
    }

    /**
     * 该方法为用户部门的实现类所使用
     * @param
     * @return
     */
    public CategoryIdModal convertByUserDepart(Category category) {
        this.key = category.getId();
        this.value = category.getId();
        this.title = category.getName();
        return this;
    }
}
