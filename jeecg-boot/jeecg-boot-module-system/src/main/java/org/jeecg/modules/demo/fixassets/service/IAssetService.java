package org.jeecg.modules.demo.fixassets.service;

import org.jeecg.modules.demo.fixassets.entity.Asset;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 固定资产表
 * @Author: jeecg-boot
 * @Date:   2020-04-02
 * @Version: V1.0
 */
public interface IAssetService extends IService<Asset> {

}
