package org.jeecg.modules.demo.fixassets.service.impl;

import org.jeecg.modules.demo.fixassets.entity.Asset;
import org.jeecg.modules.demo.fixassets.mapper.AssetMapper;
import org.jeecg.modules.demo.fixassets.service.IAssetService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 固定资产表
 * @Author: jeecg-boot
 * @Date:   2020-04-02
 * @Version: V1.0
 */
@Service
public class AssetServiceImpl extends ServiceImpl<AssetMapper, Asset> implements IAssetService {

}
