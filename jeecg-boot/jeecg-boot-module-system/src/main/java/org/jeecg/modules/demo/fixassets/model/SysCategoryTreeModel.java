package org.jeecg.modules.demo.fixassets.model;

import lombok.Data;
import org.jeecg.modules.demo.fixassets.entity.Category;
import org.jeecg.modules.system.entity.SysCategory;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.model.SysDepartTreeModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class SysCategoryTreeModel {

    private static final long serialVersionUID = 1L;

    /** 对应SysDepart中的id字段,前端数据树中的key*/
    private String key;

    /** 对应SysDepart中的id字段,前端数据树中的value*/
    private String value;

    /** 对应depart_name字段,前端数据树中的title*/
    private String title;


    private boolean isLeaf;
    // 以下所有字段均与SysDepart相同

    private String id;

    private String parentId;

    private String name;

    private List<SysCategoryTreeModel> children = new ArrayList<>();


    /**
     * 将SysCategory对象转换成SysDepartTreeModel对象
     * @param
     */
    public SysCategoryTreeModel(Category category) {
        this.key = category.getId();
        this.value = category.getId();
        this.title = category.getName();
        this.id = category.getId();
        this.parentId = category.getPid();
        this.name = category.getName();
        this.isLeaf = !Objects.equals(category.getHasChild(), "1");
        if (!this.isLeaf) {
            this.children = new ArrayList<>();
        }
    }
}
